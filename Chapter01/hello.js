var http = require('http');

http.createServer(function(req, res) {
  res.writeHead(200, {'content-type': 'text/plain'});
  res.end('Hello, world');
}).listen(3001);

console.log('Server running on port 3001');
