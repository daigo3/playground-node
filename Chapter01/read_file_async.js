var http = require('http'),
    fs = require('fs');

http.createServer(function(req, res) {
  fs.readFile('hello.js', 'utf8', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    if(err) {
      res.write('Could not find or open file for reading\n');
    } else {
      res.write(data);
    }
    res.end();
  });
}).listen(3001);

console.log('Server running on port 3001');
